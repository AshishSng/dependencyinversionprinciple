﻿using DemoLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUI
{
    public static class Factory
    {
        public static IPerson CreatePerson()
        {
            return new Person();
        }
        public static ILogger Logging()
        {
            return new Logger();
        }
        public static IEmailer SendMessage()
        {
            return new Emailer();
        }
        public static IChore CreateChore()
        {
            return new Chore(Logging(), SendMessage());
        }
    }
}
